package com.dwiki.salt.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.dwiki.salt.Model.Konsumer;

@Repository
public interface KonsumerRepository extends JpaRepository<Konsumer, String> {

}
