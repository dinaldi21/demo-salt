package com.dwiki.salt.Controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RestController;
import com.dwiki.salt.Model.Konsumer;
import com.dwiki.salt.Service.IKonsumerService;
import com.dwiki.salt.Service.KafkaConsumer;

import org.springframework.web.bind.annotation.GetMapping;



@RestController
public class KonsumerMainController {
    
    @Autowired
    private IKonsumerService konsumerService;

    @Autowired 
	private KafkaTemplate<String, Konsumer> kafkaTemplate; 

	@Autowired
	private KafkaConsumer kafkaConsumer;

	private static final String TOPIC = "NewTopic"; 

	@GetMapping(path="/konsumerKafka")
    public List<Konsumer> publishMessage() { 
        List<Konsumer> konsumers = konsumerService.getAllKonsumer();
		for (Konsumer konsumer2 : konsumers) {
        kafkaTemplate.send(TOPIC, konsumer2); 
		}
		List<Konsumer> data = kafkaConsumer.getMessages();

		kafkaTemplate.destroy();
        return data; 
    }

}
