package com.dwiki.salt.Service;

import java.util.HashMap; 
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig; 
import org.apache.kafka.common.serialization.StringSerializer; 
import org.springframework.context.annotation.Bean; 
import org.springframework.context.annotation.Configuration; 
import org.springframework.kafka.core.DefaultKafkaProducerFactory; 
import org.springframework.kafka.core.KafkaTemplate; 
import org.springframework.kafka.core.ProducerFactory; 
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.dwiki.salt.Model.Konsumer; 
  

@Configuration
public class KafkaConfig {
    @Bean
    public ProducerFactory<String, Konsumer> producerFactory() 
    { 
        Map<String, Object> config = new HashMap<>(); 
  
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092"); 
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class); 
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class); 
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
  
        return new DefaultKafkaProducerFactory<>(config); 
    } 

    @Bean
    public KafkaTemplate kafkaTemplate() 
    { 
        return new KafkaTemplate<>(producerFactory()); 
    } 
}
