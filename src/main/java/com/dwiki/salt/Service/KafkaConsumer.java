package com.dwiki.salt.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.dwiki.salt.Model.Konsumer;
import com.google.gson.Gson;

@Service
public class KafkaConsumer {
    private List<Konsumer> messages = new ArrayList<>();
        

    @KafkaListener(topics = "NewTopic", groupId = "NewGroupA")
    public void consumeMessage(String message) {
        Gson gson = new Gson();
        Konsumer konsumer = gson.fromJson(message, Konsumer.class);
        messages.add(konsumer);
    }

    public List<Konsumer> getMessages() {
        Set<Integer> set = new HashSet<>(messages.size());
		messages.removeIf(p -> !set.add(p.getId()));
        return messages;
    }
}
