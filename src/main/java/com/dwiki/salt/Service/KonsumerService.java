package com.dwiki.salt.Service;

import java.util.List;
import com.dwiki.salt.Model.Konsumer;

public interface KonsumerService {

    Konsumer getKonsumerById(String id);

    List<Konsumer> getAllKonsumer();

    Konsumer saveKonsumer(Konsumer item);

    void deleteKonsumer (String id);
}
