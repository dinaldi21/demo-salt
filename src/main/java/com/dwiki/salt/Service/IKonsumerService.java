package com.dwiki.salt.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dwiki.salt.Model.Konsumer;
import com.dwiki.salt.Repository.KonsumerRepository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
@Service
public class IKonsumerService implements KonsumerService {


    @Autowired
    private KonsumerRepository konsumerRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Konsumer getKonsumerById(String id) {
        return konsumerRepository.findById(id).orElse(null);
    }

    @Override
    public List<Konsumer> getAllKonsumer() {
        return konsumerRepository.findAll();
    }

    @Override
    public Konsumer saveKonsumer(Konsumer konsumer) {
        return konsumerRepository.save(konsumer);
    }

    @Override
    public void deleteKonsumer(String id) {
        konsumerRepository.deleteById(id);
    }
    
}
