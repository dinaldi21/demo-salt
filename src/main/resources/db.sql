CREATE TABLE `konsumer` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `kota` varchar(45) DEFAULT NULL,
  `provinsi` varchar(45) DEFAULT NULL,
  `tgl_registrasi` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO 'konsumer' (`id`, `nama`, `alamat`, `kota`, `provinsi`, `tgl_registrasi`, `status`) VALUES (1, 'Dwiki', 'Pocip', 'Bekasi', 'Jawa Barat', '2023-12-01', 'aktif');
INSERT INTO 'konsumer' (`id`, `nama`, `alamat`, `kota`, `provinsi`, `tgl_registrasi`, `status`) VALUES (2, 'Rinaldi', 'Jl. Melati', 'Jakarta Timur', 'DKI Jakarta', '2024-01-01', 'non-aktif');
INSERT INTO 'konsumer' (`id`, `nama`, `alamat`, `kota`, `provinsi`, `tgl_registrasi`, `status`) VALUES (1, 'Gege', 'Girya', 'Bekasi', 'Jawa Barat', '2022-10-01', 'aktif');