$(document).ready( function () {
    var table = $('#konsumerTable').DataTable({
           "sAjaxSource": "/konsumerKafka",
           "sAjaxDataProp": "",
           "order": [[ 0, "asc" ]],
           "aoColumns": [
                { "mData": "id"},
                { "mData": "nama" },
                { "mData": "alamat"},
                { "mData": "kota" },
                { "mData": "provinsi" },
                { "mData": "tglRegis" },
                { "mData": "status" }
           ]
    })
});